package com.bank.bankaccountsystem.validators;

import com.bank.bankaccountsystem.account.CurrencyCode;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SubAccountPLTypeValidatorImpl implements ConstraintValidator<SubAccountPLTypeValidator, List<SubAccountDto>> {

    @Override
    public boolean isValid(List<SubAccountDto> subAccountDtos, ConstraintValidatorContext constraintValidatorContext) {
        return CurrencyCode.PLN == subAccountDtos.get(0).getCurrencyCode();

    }
}
