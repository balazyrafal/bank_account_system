package com.bank.bankaccountsystem.validators;

import static com.bank.bankaccountsystem.validators.PeselUtil.decodeBirthDate;
import static com.bank.bankaccountsystem.validators.PeselUtil.isControlNumberCorrect;


import java.time.DateTimeException;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class PeselValidatorImpl implements ConstraintValidator<PeselValidator, String> {

    private static final Pattern PESEL_PATTERN = Pattern.compile("^[0-9]{11}$");
    private static final int PESEL_LENGTH = 11;

    @Override
    public boolean isValid(String pesel, ConstraintValidatorContext constraintValidatorContext) {

        if (pesel.length() != PESEL_LENGTH) {
            return false;
        }

        if (!PESEL_PATTERN.matcher(pesel).matches()) {
            return false;
        }

        try {
            decodeBirthDate(pesel);
        } catch (DateTimeException ex) {
            return false;
        }

        if (!isControlNumberCorrect(pesel)) {
            return false;
        }
        return true;
    }

}
