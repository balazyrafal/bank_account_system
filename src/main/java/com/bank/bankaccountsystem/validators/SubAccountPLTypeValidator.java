package com.bank.bankaccountsystem.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


import com.bank.bankaccountsystem.account.AccountValidationErrors;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE})
@Retention(RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = {SubAccountPLTypeValidatorImpl.class})
public @interface SubAccountPLTypeValidator {

    String message() default AccountValidationErrors.INVALID_TYPE_OF_SUB_ACCOUNT;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

