package com.bank.bankaccountsystem.validators;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class AgeLimitValidatorImpl implements ConstraintValidator<AgeLimitValidator, String> {

    @Override
    public boolean isValid(String pesel, ConstraintValidatorContext constraintValidatorContext) {
        try {
            LocalDate birthDate = PeselUtil.decodeBirthDate(pesel);
            return Period.between(birthDate, LocalDate.now()).getYears() >= 18;
        } catch (DateTimeException e) { /// method decodeBirthDate must be refctor
            return false;
        }
    }
}
