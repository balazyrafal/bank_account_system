package com.bank.bankaccountsystem.validators;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;

@UtilityClass
public class RandomTestUtils {

    public static String generateFirstName(int size) {
        return RandomStringUtils.randomAlphabetic(size);
    }

    public static String generateLastName(int size) {
        return generateFirstName(size);
    }

    public static String randomExceptionMessage() {
        return generateLastName(100);
    }
}
