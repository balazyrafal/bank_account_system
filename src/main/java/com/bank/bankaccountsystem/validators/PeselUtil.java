package com.bank.bankaccountsystem.validators;

import static org.apache.commons.lang3.StringUtils.isBlank;


import com.bank.bankaccountsystem.account.Gender;
import java.time.LocalDate;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
class PeselUtil {

    private static final int[] PESEL_WEIGHTS = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

    public static LocalDate decodeBirthDate(String peselString) {
        if (isBlank(peselString)) {
            return null;
        }

        List<Integer> pesel = parse(peselString);
        return LocalDate.of(getBirthYear(pesel), getBirthMonth(pesel), getBirthDay(pesel));
    }

    public static Gender decodeGender(String peselString) {
        if (isBlank(peselString)) {
            return null;
        }

        List<Integer> pesel = parse(peselString);
        return getGender(pesel);
    }

    private static List<Integer> parse(String peselString) {
        return peselString.chars()
            .mapToObj(Character::getNumericValue)
            .toList();
    }

    private static int getBirthYear(List<Integer> pesel) {
        int year;
        int month;
        year = 10 * pesel.get(0);
        year += pesel.get(1);
        month = 10 * pesel.get(2);
        month += pesel.get(3);
        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        } else {
            year += 1900;
        }

        return year;
    }

    private static int getBirthMonth(List<Integer> pesel) {
        int month;
        month = 10 * pesel.get(2);
        month += pesel.get(3);
        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    private static int getBirthDay(List<Integer> pesel) {
        int day;
        day = 10 * pesel.get(4);
        day += pesel.get(5);
        return day;
    }

    public static boolean isControlNumberCorrect(String pesel) {
        List<Integer> peselAsDigits = parse(pesel);
        return peselAsDigits.get(10) == calculateControlNumber(peselAsDigits);
    }

    private static int calculateControlNumber(List<Integer> pesel) {
        int controlSum = 0;
        for (int i = 0; i < PESEL_WEIGHTS.length; i++) {
            controlSum += getRemainder(pesel.get(i) * PESEL_WEIGHTS[i]);
        }
        final int sumRemainder = getRemainder(controlSum);
        return sumRemainder == 0 ? sumRemainder : 10 - sumRemainder;
    }

    private static int getRemainder(int number) {
        return number % 10;
    }

    private static Gender getGender(List<Integer> pesel) {
        if (pesel.get(9) % 2 == 1) {
            return Gender.M;
        } else {
            return Gender.F;
        }
    }

}
