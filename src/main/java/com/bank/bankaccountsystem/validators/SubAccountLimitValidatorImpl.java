package com.bank.bankaccountsystem.validators;

import com.bank.bankaccountsystem.shared.SubAccountDto;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SubAccountLimitValidatorImpl implements ConstraintValidator<SubAccountLimitValidator, List<SubAccountDto>> {

    @Override
    public boolean isValid(List<SubAccountDto> subAccountDtos, ConstraintValidatorContext constraintValidatorContext) {
        return subAccountDtos.size() == 1;
    }

}
