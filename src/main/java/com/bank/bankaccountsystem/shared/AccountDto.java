package com.bank.bankaccountsystem.shared;

import com.bank.bankaccountsystem.account.AccountValidationErrors;
import com.bank.bankaccountsystem.account.CurrencyCode;
import com.bank.bankaccountsystem.validators.AgeLimitValidator;
import com.bank.bankaccountsystem.validators.PeselValidator;
import com.bank.bankaccountsystem.validators.SubAccountLimitValidator;
import com.bank.bankaccountsystem.validators.SubAccountPLTypeValidator;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class AccountDto implements Cloneable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 255, message = AccountValidationErrors.FIRST_NAME_NO_LIMIT)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 255, message = AccountValidationErrors.LAST_NAME_NO_LIMIT)
    private String lastName;

    @NotNull
    @AgeLimitValidator
    @PeselValidator
    private String pesel;

    @NotNull
    @SubAccountLimitValidator
    @SubAccountPLTypeValidator
    private List<SubAccountDto> subAccounts;

    @Version
    private Long version;

    public AccountDto(Long id, String firstName, String lastName, String pesel, List<SubAccountDto> subAccounts) { /// celowo lista zostaiona na potrzeby dalszego rozwoju, np. więcej kont itd.
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.subAccounts = new ArrayList<>(subAccounts.size());
        this.subAccounts.addAll(subAccounts);

    }

    public static AccountDto from(String firstName, String lastName, String pesel, List<SubAccountDto> subAccounts) {
        return new AccountDto(null, firstName, lastName, pesel, subAccounts);
    }

    public SubAccountDto getSubAccountBy(CurrencyCode sourceCurrencyCode) {
        List<SubAccountDto> subAccountDtos = this.getSubAccounts().stream()
            .filter(subAccountDto -> subAccountDto.getCurrencyCode() == sourceCurrencyCode)
            .collect(Collectors.toList());

        if (subAccountDtos.size() > 1) {
            throw new RuntimeException("Wrong account state. Sth implement problem"); // TODO: 10.06.2024 need better solution
        }

        return subAccountDtos.stream().findFirst().get();

    }

    @Override
    public AccountDto clone() throws CloneNotSupportedException {
        AccountDto accountDto = new AccountDto();
        accountDto.firstName = this.firstName;
        accountDto.lastName = this.lastName;
        accountDto.pesel = this.pesel;
        accountDto.subAccounts = subAccounts.stream().map(subAccountDto -> {
            try {
                return subAccountDto.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());

        return AccountDto.from(firstName, lastName, pesel, subAccounts);
    }
}