package com.bank.bankaccountsystem.shared;

import com.bank.bankaccountsystem.account.CurrencyCode;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SubAccountDto implements Cloneable {

    private CurrencyCode currencyCode;
    private BigDecimal amount;

    public static SubAccountDto pln(BigDecimal amount) {
        return new SubAccountDto(CurrencyCode.PLN, amount);
    }

    public static SubAccountDto usd(BigDecimal amount) {
        return new SubAccountDto(CurrencyCode.USD, amount);
    }

    public void changeAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SubAccountDto clone() throws CloneNotSupportedException {
        return new SubAccountDto(this.currencyCode, this.amount);
    }
}