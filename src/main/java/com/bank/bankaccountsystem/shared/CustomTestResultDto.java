package com.bank.bankaccountsystem.shared;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomTestResultDto {

    private HttpStatus status;
    private String message;

    public static CustomTestResultDto CustomTestResultDto(HttpStatus httpStatus, String message) {
        return new CustomTestResultDto(httpStatus, message);
    }

}
