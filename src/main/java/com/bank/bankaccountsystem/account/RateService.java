package com.bank.bankaccountsystem.account;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class RateService {

    private final CurrencyExchangeRateRepository currencyExchangeRateRepository;

    BigDecimal getCurrencyRate(CurrencyCode source, CurrencyCode destination, LocalDate effectiveDate) {

        if (source == destination) {
            return BigDecimal.ONE;
        }

        Optional<CurrencyExchangeRate> sourceRateFor = currencyExchangeRateRepository.findRateFor(source, effectiveDate);
        Optional<CurrencyExchangeRate> destinmationRateFor = currencyExchangeRateRepository.findRateFor(destination, effectiveDate);

        if (sourceRateFor.isEmpty() && destinmationRateFor.isEmpty()) {
            throw new NoCurrencyRateAvaliableException(String.format("no rate for %s and effective date %s", source, effectiveDate));
        }

        BigDecimal sourceRate = sourceRateFor.map(CurrencyExchangeRate::getMid).orElse(BigDecimal.ONE);
        BigDecimal destinationRate = destinmationRateFor.map(CurrencyExchangeRate::getMid).orElse(BigDecimal.ONE);

        return destinationRate.divide(sourceRate).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

}
