package com.bank.bankaccountsystem.account;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table
@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder
public class CurrencyExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long currId;
    private String nbpTable;
    private String currencyDescription;
    @Enumerated(EnumType.STRING)
    private CurrencyCode currencyCode;
    private LocalDate effectiveDate;
    private BigDecimal mid;

}
