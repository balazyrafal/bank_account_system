package com.bank.bankaccountsystem.account;

class AccountFoundException extends RuntimeException {
    public AccountFoundException(String message) {
        super(message);
    }
}
