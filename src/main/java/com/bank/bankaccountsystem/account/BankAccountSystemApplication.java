package com.bank.bankaccountsystem.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.bank.bankaccountsystem.account")
public class BankAccountSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAccountSystemApplication.class, args);
    }

}
