package com.bank.bankaccountsystem.account;

public class NoAccountMoneyNeedToOperationException extends RuntimeException {

    public NoAccountMoneyNeedToOperationException(String message) {
        super(message);
    }
}
