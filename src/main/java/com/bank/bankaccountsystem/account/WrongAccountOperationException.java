package com.bank.bankaccountsystem.account;

public class WrongAccountOperationException extends RuntimeException {

    public WrongAccountOperationException(String message) {
        super(message);
    }
}
