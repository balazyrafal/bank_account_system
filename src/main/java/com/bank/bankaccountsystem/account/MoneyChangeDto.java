package com.bank.bankaccountsystem.account;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class MoneyChangeDto {

    @NotNull
    private String pesel;
    @NotNull
    private BigDecimal amount;
    @NotNull
    private CurrencyCode sourceCurrencyCode;
    @NotNull
    private CurrencyCode destinationCurrencyCode;

}
