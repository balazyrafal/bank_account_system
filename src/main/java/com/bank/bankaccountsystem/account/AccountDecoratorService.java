package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import org.springframework.stereotype.Component;

@Component
public class AccountDecoratorService {

    AccountDto decorateExtraCurrencyAccount(CurrencySubAccount currencuSubAccount)
        throws CloneNotSupportedException {

        AccountDto newAccountDto = currencuSubAccount.accountDto().clone();
        if (CurrencyCode.USD == currencuSubAccount.currencyCode()) {
            newAccountDto.getSubAccounts().add(SubAccountDto.usd(currencuSubAccount.amount()));
            return newAccountDto;
        }
        if (CurrencyCode.PLN == currencuSubAccount.currencyCode()) {
            newAccountDto.getSubAccounts().add(SubAccountDto.pln(currencuSubAccount.amount()));
            return newAccountDto;
        }

        throw new IllegalStateException("todo ");
    }

}
