package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.RateDto;
import java.util.List;

public class NbpRateDto {

    private String table;
    private String currency;
    private String code;
    private List<RateDto> rates;

}
