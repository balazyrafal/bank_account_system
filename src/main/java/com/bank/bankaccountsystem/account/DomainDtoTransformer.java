package com.bank.bankaccountsystem.account;

import java.util.List;
import java.util.stream.Collectors;

public abstract class DomainDtoTransformer<Dto, Domain> {

    abstract Dto toDto(Domain domain);

    abstract Domain toDomain(Dto dto);

    List<Dto> toDtos(List<Domain> domains) {
        return domains.stream().map(this::toDto).collect(Collectors.toList());
    }

    List<Domain> toDomains(List<Dto> dtos) {
        return dtos.stream().map(this::toDomain).collect(Collectors.toList());
    }
}
