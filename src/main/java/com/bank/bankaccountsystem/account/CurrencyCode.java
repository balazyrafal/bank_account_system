package com.bank.bankaccountsystem.account;

public enum CurrencyCode {

    PLN,
    USD;
}
