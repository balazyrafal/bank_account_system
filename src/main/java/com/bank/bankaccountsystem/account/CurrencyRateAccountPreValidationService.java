package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import java.math.BigDecimal;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Component
class CurrencyRateAccountPreValidationService {

    void validateAndThrowIfNoMoneyNeedToOperation(MoneyChangeDto moneyChangeDto, Optional<AccountDto> accountDto) {
        BigDecimal operationAmount = moneyChangeDto.getAmount();
        SubAccountDto sourceSubAccount = accountDto.get().getSubAccountBy(moneyChangeDto.getSourceCurrencyCode());

        if (sourceSubAccount.getAmount().compareTo(operationAmount) < 0) {
            throw new NoAccountMoneyNeedToOperationException(AccountValidationErrors.NO_MONEY_NEED_TO_OPERATION);
        }
    }

    void validateAndThrowIfPlannedWrongOperation(MoneyChangeDto moneyChangeDto) {
        CurrencyCode sourceCurrencyCode = moneyChangeDto.getSourceCurrencyCode();
        CurrencyCode destinationCurrencyCode = moneyChangeDto.getDestinationCurrencyCode();
        if (sourceCurrencyCode == destinationCurrencyCode) {
            throw new WrongAccountOperationException(AccountValidationErrors.WRONG_EXPECTED_OPERATION);
        }
    }

    void validateAndThrowIfAccountNoExits(Optional<AccountDto> accountDto) {
        if (accountDto.isEmpty()) {
            throw new AccountNotFoundException(AccountValidationErrors.WRONG_EXPECTED_OPERATION);
        }
    }
}
