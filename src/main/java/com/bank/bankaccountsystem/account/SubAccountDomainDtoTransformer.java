package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.SubAccountDto;
import org.springframework.stereotype.Component;

@Component
class SubAccountDomainDtoTransformer extends DomainDtoTransformer<SubAccountDto, SubAccount> {

    @Override
    SubAccountDto toDto(SubAccount subAccount) {
        return SubAccountDto.builder()
            .currencyCode(subAccount.getCurrencyCode())
            .amount(subAccount.getAmount())
            .build();
    }

    @Override
    SubAccount toDomain(SubAccountDto subAccountDto) {
        return SubAccount.builder()
            .currencyCode(subAccountDto.getCurrencyCode())
            .amount(subAccountDto.getAmount())
            .build();
    }
}
