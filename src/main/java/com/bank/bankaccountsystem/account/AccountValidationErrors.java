package com.bank.bankaccountsystem.account;

public interface AccountValidationErrors {

    String FIRST_NAME_NO_LIMIT = "First name length must be in <2, 255>";
    String LAST_NAME_NO_LIMIT = "Last  name length must be in <2, 255>";
    String INVALID_PESEL = "INVALID PESEL";
    String TO_MANY_SUB_ACCOUNTS = "to many subaccounts (required 1 only)";
    String INVALID_TYPE_OF_SUB_ACCOUNT = "required PLN sub_account on init";
    String AGE_LESS_THAN_18 = "age less than 18";
    String ACCOUNT_EXISTS = "account exists";
    String WRONG_EXPECTED_OPERATION = "wrong operation";
    String NO_MONEY_NEED_TO_OPERATION = "no money operation";
}
