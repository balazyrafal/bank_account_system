package com.bank.bankaccountsystem.account;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByPesel(String pesel);
}
