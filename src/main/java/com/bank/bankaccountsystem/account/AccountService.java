package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import java.math.BigDecimal;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@AllArgsConstructor
@Service
class AccountService {

    private final AccountRepository accountRepository;
    private final AccountDomainDtoResponseTransformer accountDomainDtoResponseTransformer;
    private final AccountDecoratorService accountDecoratorService;

    @SneakyThrows
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AccountDto create(AccountDto accountDto) {

        checkAndThrowIfAccountByPeselExists(accountDto.getPesel());
        CurrencySubAccount currencySubAccount = new CurrencySubAccount(accountDto, CurrencyCode.USD, BigDecimal.ZERO);
        AccountDto accountDtoToSave = accountDecoratorService.decorateExtraCurrencyAccount(currencySubAccount);
        Account accountToSave = accountDomainDtoResponseTransformer.toDomain(accountDtoToSave);
        Account accountSaved = accountRepository.save(accountToSave);
        return accountDomainDtoResponseTransformer.toDto(accountSaved);
    }

    public Account update(Account account){
        return accountRepository.save(account);
    }


    @Transactional
    Optional<AccountDto> findByPesel(String pesel) {
        return accountRepository.findByPesel(pesel)
            .map(bankAccount -> accountDomainDtoResponseTransformer.toDto(bankAccount));
    }

    private void checkAndThrowIfAccountByPeselExists(String pesel) {
        boolean present = accountRepository.findByPesel(pesel).isPresent();
        if (present) {
            throw new AccountFoundException(String.format(AccountValidationErrors.ACCOUNT_EXISTS));
        }
    }

    ///onlu for test
    void deleteAll() {
        accountRepository.deleteAll();
    }
}
