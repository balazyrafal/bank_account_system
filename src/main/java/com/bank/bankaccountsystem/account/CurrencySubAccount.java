package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import java.math.BigDecimal;

public record CurrencySubAccount(AccountDto accountDto, CurrencyCode currencyCode, BigDecimal amount) {

}