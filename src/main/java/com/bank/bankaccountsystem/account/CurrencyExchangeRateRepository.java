package com.bank.bankaccountsystem.account;

import java.time.LocalDate;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

interface CurrencyExchangeRateRepository extends JpaRepository<CurrencyExchangeRate, Long> {

    @Query("select cer From CurrencyExchangeRate  cer where cer.currencyCode = :currencyCode and cer.effectiveDate = :effectiveDate")
    Optional<CurrencyExchangeRate> findRateFor(CurrencyCode currencyCode, LocalDate effectiveDate);
}
