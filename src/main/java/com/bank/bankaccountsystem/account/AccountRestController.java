package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping("/api/v1")
@RestController
public class AccountRestController {

    private final AccountService accountService;
    private final CurrencyRateAccountOperationService currencyRateAccountOperationService;

    @GetMapping(value = "/account/{pesel}/info")
    public AccountDto accountByPesel(@PathVariable("pesel") String pesel) {
        return accountService.findByPesel(pesel)
            .orElseThrow(() -> new AccountNotFoundException(String.format("Not found account by pesel = [%s]", pesel)));
    }

    @PostMapping(value = "/account", consumes = "application/json", produces = "application/json")
    public AccountDto create(@Valid @RequestBody AccountDto accountDto) {
        return accountService.create(accountDto);
    }

    @PutMapping(value = "/account/{pesel}/{sourceCurrencyCode}/to/{destinationCurrencyCode}")
    public AccountDto moneyExchange(@PathVariable("pesel") String pesel,
                                    @PathVariable("sourceCurrencyCode") CurrencyCode sourceCurrencyCode,
                                    @PathVariable("destinationCurrencyCode") CurrencyCode destinationCurrencyCode,
                                    @PathParam("amount") BigDecimal amount) {
        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(pesel)
            .sourceCurrencyCode(sourceCurrencyCode)
            .destinationCurrencyCode(destinationCurrencyCode)
            .amount(amount)
            .build();
        return currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto);
    }

}
