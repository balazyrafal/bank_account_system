package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import com.bank.bankaccountsystem.validators.RandomTestUtils;
import java.math.BigDecimal;
import java.util.List;

public class AccountTestFactory {

    public static String validPesel = "11111111111";
    public static String validPeselWithAgeLessThan18 = "18241226741";
    public static String invalidPesel = "00000000000";

    public static AccountDto prepareWithFirstNameLessThanMinSize() {
        return AccountDto.from(RandomTestUtils.generateFirstName(1), RandomTestUtils.generateLastName(255), validPesel, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }

    public static AccountDto prepareWithFirstNameLessThanMaxSize() {
        return AccountDto.from(RandomTestUtils.generateFirstName(1000), RandomTestUtils.generateLastName(255), validPesel, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }

    public static Object prepareWithLastNameLessThanMinSize() {
        return AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(1), validPesel, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }

    public static Object prepareWithLastNameLessThanMaxSize() {
        return AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(1000), validPesel, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }

    public static AccountDto prepareWithAgeLessThan18() {
        return AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), validPeselWithAgeLessThan18, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }

    public static AccountDto prepareWithWrongPesel() {
        return AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), invalidPesel, List.of(SubAccountDto.pln(BigDecimal.valueOf(1000))));
    }
}
