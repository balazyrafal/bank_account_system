package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Component
public class CurrencyRateAccountOperationService {

    private final AccountService accountService;
    private final CurrencyRateAccountPreValidationService preValidationService;
    private final RateService rateService;
    private final AccountDomainDtoResponseTransformer accountDomainDtoResponseTransformer;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AccountDto tryToCurrencyExchangeBetweenSubAccounts(MoneyChangeDto moneyChangeDto) {

        Optional<AccountDto> accountDto = accountService.findByPesel(moneyChangeDto.getPesel());

        preValidationService.validateAndThrowIfAccountNoExits(accountDto);
        preValidationService.validateAndThrowIfPlannedWrongOperation(moneyChangeDto);
        preValidationService.validateAndThrowIfNoMoneyNeedToOperation(moneyChangeDto, accountDto);

        AccountDto accountAfterChanged = changeAmountInAccounts(moneyChangeDto, accountDto.get());

        Account domain = accountDomainDtoResponseTransformer.toDomain(accountAfterChanged);
        accountService.update(domain);

        String pesel = moneyChangeDto.getPesel();
        return accountService.findByPesel(pesel)
            .orElseThrow(() -> new AccountNotFoundException(String.format("Not found account by pesel = [%s]", pesel)));
    }

    private AccountDto changeAmountInAccounts(MoneyChangeDto moneyChangeDto, AccountDto accountDto) {

        CurrencyCode sourceCurrencyCode = moneyChangeDto.getSourceCurrencyCode();
        CurrencyCode destinationCurrencyCode = moneyChangeDto.getDestinationCurrencyCode();

        SubAccountDto sourceSubAccount = accountDto.getSubAccountBy(sourceCurrencyCode);
        SubAccountDto destinationSubAccount = accountDto.getSubAccountBy(destinationCurrencyCode);

        BigDecimal calculatedAmount = calculateAmount(moneyChangeDto.getAmount(), sourceCurrencyCode, destinationCurrencyCode);

        if (calculatedAmount.compareTo(BigDecimal.ZERO) == 0) {
            return accountDto;
        }

        sourceSubAccount.changeAmount(sourceSubAccount.getAmount().subtract(moneyChangeDto.getAmount()));
        destinationSubAccount.changeAmount(destinationSubAccount.getAmount().add(calculatedAmount));

        return accountDto;
    }

    private BigDecimal calculateAmount(BigDecimal amount, CurrencyCode sourceCurrencyCode, CurrencyCode destinationCurrencyCode) {
        BigDecimal rate = rateService.getCurrencyRate(sourceCurrencyCode, destinationCurrencyCode, LocalDate.now()); // TODO: 10.06.2024 change localdate time
        return amount.multiply(rate).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

}
