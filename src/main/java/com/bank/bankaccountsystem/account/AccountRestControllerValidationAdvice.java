package com.bank.bankaccountsystem.account;

import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class AccountRestControllerValidationAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream()
            .map(fieldError -> fieldError.getObjectName() + " " + fieldError.getDefaultMessage() + System.lineSeparator())
            .collect(Collectors.joining(","));
    }

    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String accountNotFoundExceptionHandler(AccountNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(AccountFoundException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String accountFoundExceptionHandler(AccountFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(WrongAccountOperationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
        // TODO: 10.06.2024 http status do dyskusji
    String wrongAccountOperationExceptionHandler(WrongAccountOperationException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(NoAccountMoneyNeedToOperationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
        // TODO: 10.06.2024 http status do dyskusji
    String noAccountMoneyNeedToOperationExceptionHandler(NoAccountMoneyNeedToOperationException ex) {
        return ex.getMessage();
    }

}
