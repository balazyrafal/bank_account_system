package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.SubAccountDto;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AccountResponseDto {

    private String firstName;

    private String lastName;

    private String pesel;

    private List<SubAccountDto> subAccounts;

}