package com.bank.bankaccountsystem.account;

public class NoCurrencyRateAvaliableException extends RuntimeException {

    public NoCurrencyRateAvaliableException(String message) {
        super(message);
    }
}
