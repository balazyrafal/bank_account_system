package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
class AccountDomainDtoResponseTransformer extends DomainDtoTransformer<AccountDto, Account> {

    private final SubAccountDomainDtoTransformer subAccountDomainDtoTransformer;

    @Override
    AccountDto toDto(Account bankAccount) {
        return new AccountDto(bankAccount.getId(), bankAccount.getFirstName(),
            bankAccount.getLastName(), bankAccount.getPesel(), subAccountDomainDtoTransformer.toDtos(bankAccount.getSubAccounts()));
    }

    @Override
    Account toDomain(AccountDto accountDto) {
        return Account.builder()
            .id(accountDto.getId())
            .firstName(accountDto.getFirstName())
            .lastName(accountDto.getLastName())
            .pesel(accountDto.getPesel())
            .subAccounts(subAccountDomainDtoTransformer.toDomains(accountDto.getSubAccounts()))
            .build();
    }
}