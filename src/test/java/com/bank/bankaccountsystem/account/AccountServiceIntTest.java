package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import com.bank.bankaccountsystem.validators.RandomTestUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountServiceIntTest {

    @Autowired
    private AccountService accountService;

    @BeforeEach
    public void clean() {
        accountService.deleteAll();
    }

    @Test
    void shouldCreateAccountWhenTryCreate() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        ///when
        accountService.create(accountDto);

        ///then
        Optional<AccountDto> accountDtoOpt = accountService.findByPesel(accountDto.getPesel());
        assertTrue(accountDtoOpt.isPresent());

        AccountDto accountDtoAfterSave = accountDtoOpt.get();
        assertEquals(2, accountDtoAfterSave.getSubAccounts().size());

        List<SubAccountDto> subAccountDtosInPLN = accountDtoAfterSave.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.PLN == subAccountDto.getCurrencyCode()).collect(Collectors.toList());
        List<SubAccountDto> subAccountDtosInUSD = accountDtoAfterSave.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.USD == subAccountDto.getCurrencyCode()).collect(Collectors.toList());

        assertTrue(subAccountDtosInPLN.size() == 1);
        assertTrue(subAccountDtosInPLN.get(0).getAmount().compareTo(amountInPL) == 0);
        assertTrue(subAccountDtosInUSD.size() == 1);
        assertTrue(subAccountDtosInUSD.get(0).getAmount().compareTo(BigDecimal.ZERO) == 0);

    }

    @Test
    void shouldThrowExceptionWhenTryCreateAccountUseExistsPesel() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        ///when
        accountService.create(accountDto);

        ///then
        Optional<AccountDto> accountDtoOpt = accountService.findByPesel(accountDto.getPesel());
        assertTrue(accountDtoOpt.isPresent());
        assertThrows(AccountFoundException.class, () -> accountService.create(accountDto));

    }

}