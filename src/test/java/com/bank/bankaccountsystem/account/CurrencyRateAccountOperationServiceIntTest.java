package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import com.bank.bankaccountsystem.validators.RandomTestUtils;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CurrencyRateAccountOperationServiceIntTest {

    @MockBean
    private RateService rateServiceMock;

    @Autowired
    private AccountService accountService;
    @Autowired
    private CurrencyRateAccountOperationService currencyRateAccountOperationService;

    @BeforeEach
    public void clean() {
        accountService.deleteAll();
    }

    @Test
    void shouldTryExchangeFromPlNtoUSD() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        /// mock rate service
        Mockito.when(rateServiceMock.getCurrencyRate(eq(CurrencyCode.PLN), eq(CurrencyCode.USD), any(LocalDate.class))).thenReturn(BigDecimal.valueOf(0.25)); // PLN>USD

        ///when
        accountService.create(tmpAccountDto);
        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.TEN)
            .sourceCurrencyCode(CurrencyCode.PLN)
            .destinationCurrencyCode(CurrencyCode.USD)
            .build();

        AccountDto accountDto = currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto);

        List<SubAccountDto> subAccountDtosInPLN = accountDto.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.PLN == subAccountDto.getCurrencyCode()).collect(Collectors.toList());
        List<SubAccountDto> subAccountDtosInUSD = accountDto.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.USD == subAccountDto.getCurrencyCode()).collect(Collectors.toList());

        ///then
        assertEquals(1, subAccountDtosInPLN.size());
        assertEquals(1, subAccountDtosInUSD.size());
        assertEquals(0, subAccountDtosInPLN.get(0).getAmount().compareTo(BigDecimal.valueOf(990)));
        assertEquals(0, subAccountDtosInUSD.get(0).getAmount().compareTo(BigDecimal.valueOf(2.50)));

    }

    @Test
    void shouldThrowExceptionWhenTryExchangeFromUSDtoPLNWhenUSDNoMoney() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        /// mock rate service
        Mockito.when(rateServiceMock.getCurrencyRate(eq(CurrencyCode.USD), eq(CurrencyCode.PLN), any(LocalDate.class))).thenReturn(BigDecimal.valueOf(4)); // PLN>USD

        ///when
        accountService.create(tmpAccountDto);
        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.TEN)
            .sourceCurrencyCode(CurrencyCode.USD)
            .destinationCurrencyCode(CurrencyCode.PLN)
            .build();

        assertThrows(NoAccountMoneyNeedToOperationException.class, () -> currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto));

    }

    @Test
    void shouldTryExchangeFromUSDtoPLN() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(10000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        /// mock rate service
        Mockito.when(rateServiceMock.getCurrencyRate(eq(CurrencyCode.PLN), eq(CurrencyCode.USD), any(LocalDate.class))).thenReturn(BigDecimal.valueOf(0.25)); // PLN>USD
        Mockito.when(rateServiceMock.getCurrencyRate(eq(CurrencyCode.USD), eq(CurrencyCode.PLN), any(LocalDate.class))).thenReturn(BigDecimal.valueOf(4)); // PLN>USD

        ///when
        accountService.create(tmpAccountDto);
        MoneyChangeDto moneyChangeDtoTransferPlnToUSD = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.valueOf(1000))
            .sourceCurrencyCode(CurrencyCode.PLN)
            .destinationCurrencyCode(CurrencyCode.USD)
            .build();

        AccountDto accountDto1 = currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDtoTransferPlnToUSD);

        MoneyChangeDto moneyChangeDtoTransferUSDtoPLN = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.valueOf(200))
            .sourceCurrencyCode(CurrencyCode.USD)
            .destinationCurrencyCode(CurrencyCode.PLN)
            .build();

        AccountDto accountDto = currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDtoTransferUSDtoPLN);
        ///then
        List<SubAccountDto> subAccountDtosInPLN = accountDto.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.PLN == subAccountDto.getCurrencyCode()).collect(Collectors.toList());
        List<SubAccountDto> subAccountDtosInUSD = accountDto.getSubAccounts().stream().filter(subAccountDto -> CurrencyCode.USD == subAccountDto.getCurrencyCode()).collect(Collectors.toList());
        assertEquals(1, subAccountDtosInPLN.size());
        assertEquals(1, subAccountDtosInUSD.size());
        assertEquals(0, subAccountDtosInPLN.get(0).getAmount().compareTo(BigDecimal.valueOf(9800)));
        assertEquals(0, subAccountDtosInUSD.get(0).getAmount().compareTo(BigDecimal.valueOf(50)));

    }

    @Test
    void shouldThrowIfOperationAccountNoExists() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        ///when

        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.TEN)
            .sourceCurrencyCode(CurrencyCode.PLN)
            .destinationCurrencyCode(CurrencyCode.USD)
            .build();

        //then
        assertThrows(AccountNotFoundException.class, () -> currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto));

    }

    @Test
    void shouldThrowWhenPlannedWrongOperation() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        ///when
        accountService.create(tmpAccountDto);
        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.TEN)
            .sourceCurrencyCode(CurrencyCode.PLN)
            .destinationCurrencyCode(CurrencyCode.PLN)
            .build();

        //then
        assertThrows(WrongAccountOperationException.class, () -> currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto));

    }

    @Test
    void shouldThrowWhenNoMoneyForOperation() {
        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto tmpAccountDto = AccountDto.from(RandomTestUtils.generateFirstName(100), RandomTestUtils.generateLastName(100), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        ///when
        accountService.create(tmpAccountDto);
        MoneyChangeDto moneyChangeDto = MoneyChangeDto.builder()
            .pesel(tmpAccountDto.getPesel())
            .amount(BigDecimal.valueOf(1001))
            .sourceCurrencyCode(CurrencyCode.PLN)
            .destinationCurrencyCode(CurrencyCode.USD)
            .build();

        //then
        assertThrows(NoAccountMoneyNeedToOperationException.class, () -> currencyRateAccountOperationService.tryToCurrencyExchangeBetweenSubAccounts(moneyChangeDto));

    }
}