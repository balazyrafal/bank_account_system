package com.bank.bankaccountsystem.account;

import com.bank.bankaccountsystem.shared.AccountDto;
import com.bank.bankaccountsystem.shared.CustomTestResultDto;
import com.bank.bankaccountsystem.shared.SubAccountDto;
import com.bank.bankaccountsystem.validators.RandomTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AccountRestController.class)
class AccountRestControllerTest {

    private final static String URI = "http://localhost:8080"; // TODO: 09.06.2024 given from configuration
    @MockBean
    private AccountService accountServiceMock;
    @MockBean
    private CurrencyRateAccountOperationService currencyRateAccountOperationServiceMock;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @MethodSource("testCases")
    public void shouldReturnValidationErrors(AccountDto accountDto, CustomTestResultDto customTestResultDto) throws Exception {

        //given
        Mockito.when(accountServiceMock.create(any(AccountDto.class))).thenReturn(accountDto);

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(URI + "/api/v1/account").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        //then
        resultActions.andExpect(MockMvcResultMatchers.status().is(customTestResultDto.getStatus().value())).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON)).andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException)).andExpect(result -> assertTrue(result.getResponse().getContentAsString().contains(customTestResultDto.getMessage())));

    }

    @Test
    public void shouldCreateAccountWhenAllDataIsValid() throws Exception {

        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        Mockito.when(accountServiceMock.create(any(AccountDto.class))).thenReturn(accountDto);

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(URI + "/api/v1/account").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        //then
        resultActions.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(accountDto.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(accountDto.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pesel").value(accountDto.getPesel()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.subAccounts[0].currencyCode").value(accountDto.getSubAccounts().get(0).getCurrencyCode().name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.subAccounts[0].amount").value(accountDto.getSubAccounts().get(0).getAmount()));
    }

    @Test
    void shouldReturnAccountInfoWhenPeselExists() throws Exception {

        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        Mockito.when(accountServiceMock.findByPesel(any(String.class))).thenReturn(Optional.ofNullable(accountDto));

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(URI + "/api/v1/account/1111111111118/info").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        //then
        resultActions.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnNoDataAndErrorInfoProperlyWhenPeselNoExists() throws Exception {

        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.pln(amountInPL)));

        String randomExceptionMessage = RandomTestUtils.randomExceptionMessage();
        Mockito.when(accountServiceMock.findByPesel(any(String.class))).thenThrow(new AccountNotFoundException(randomExceptionMessage));

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(URI + "/api/v1/account/00000000000/info").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(MockMvcResultMatchers.status().isNotFound()).andExpect(result -> assertTrue(result.getResolvedException() instanceof AccountNotFoundException)).andExpect(result -> assertEquals(randomExceptionMessage, result.getResolvedException().getMessage()));

    }

    @Test
    void shouldReturnBadRequestWhenWrongSubAccountRequired() throws Exception {

        //given
        BigDecimal amountInPL = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.usd(amountInPL)));

        Mockito.when(accountServiceMock.findByPesel(any(String.class))).thenReturn(Optional.ofNullable(accountDto));

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(URI + "/api/v1/account").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest()).andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException)).andExpect(result -> assertTrue(result.getResponse().getContentAsString().contains(AccountValidationErrors.INVALID_TYPE_OF_SUB_ACCOUNT)));

    }

    @Test
    void shouldReturnBadRequestWhenTooManySubAccountsThatRequired() throws Exception {

        //given
        BigDecimal amount = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.usd(amount), SubAccountDto.pln(amount)));

        Mockito.when(accountServiceMock.findByPesel(any(String.class))).thenReturn(Optional.ofNullable(accountDto));

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(URI + "/api/v1/account").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest()).andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException)).andExpect(result -> assertTrue(result.getResponse().getContentAsString().contains(AccountValidationErrors.TO_MANY_SUB_ACCOUNTS)));

    }

    @Test
    void shouldReturnConflictWhenAccountByPeselExists() throws Exception {

        //given
        BigDecimal amount = BigDecimal.valueOf(1000);
        AccountDto accountDto = AccountDto.from(RandomTestUtils.generateFirstName(255), RandomTestUtils.generateLastName(255), "11111111111", List.of(SubAccountDto.pln(amount)));

        Mockito.when(accountServiceMock.create(any(AccountDto.class))).thenThrow(new AccountFoundException(AccountValidationErrors.ACCOUNT_EXISTS));

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(URI + "/api/v1/account").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(accountDto)).accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(MockMvcResultMatchers.status().isConflict()).andExpect(result -> assertTrue(result.getResolvedException() instanceof AccountFoundException)).andExpect(result -> assertTrue(result.getResponse().getContentAsString().contains(AccountValidationErrors.ACCOUNT_EXISTS)));

    }

    //// odpaliłem tylko test zeby sprawdzić put na rest
    /// mam ograniczony czas
    @ParameterizedTest
    @MethodSource("moneyExchangeTestCases")
    public void shouldTestMoneyExchangeByTestCases(String pesel,
                                                   CurrencyCode sourceCurrencyCode,
                                                   CurrencyCode destinationCurrencyCode,
                                                   BigDecimal amount,
                                                   HttpStatus httpStatus) throws Exception {

        String path = "/api/v1/account/#pesel/#sourceCurrencyCode/to/#destinationCurrencyCode?amount=#amount";

        path = path.replaceAll("#pesel", pesel);
        path = path.replaceAll("#sourceCurrencyCode", sourceCurrencyCode.name());
        path = path.replaceAll("#destinationCurrencyCode", destinationCurrencyCode.name());
        path = path.replaceAll("#amount", amount.toString());

        //when
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put(URI + path)
                .accept(MediaType.APPLICATION_JSON));

        resultActions.andExpect(MockMvcResultMatchers.status().is(httpStatus.value()));

    }

    private static Stream<Arguments> testCases() {
        return Stream.of(Arguments.of(AccountTestFactory.prepareWithFirstNameLessThanMinSize(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.FIRST_NAME_NO_LIMIT)),

                Arguments.of(AccountTestFactory.prepareWithFirstNameLessThanMaxSize(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.FIRST_NAME_NO_LIMIT)),

                Arguments.of(AccountTestFactory.prepareWithLastNameLessThanMinSize(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.LAST_NAME_NO_LIMIT)),

                Arguments.of(AccountTestFactory.prepareWithLastNameLessThanMaxSize(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.LAST_NAME_NO_LIMIT)),

                Arguments.of(AccountTestFactory.prepareWithAgeLessThan18(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.AGE_LESS_THAN_18)),

                Arguments.of(AccountTestFactory.prepareWithWrongPesel(), CustomTestResultDto.CustomTestResultDto(HttpStatus.BAD_REQUEST, AccountValidationErrors.INVALID_PESEL))

        );
    }

    private static Stream<Arguments> moneyExchangeTestCases() {
        return Stream.of(Arguments.of("1111111111118", CurrencyCode.PLN, CurrencyCode.USD, BigDecimal.ONE, HttpStatus.OK));
    }

}