package com.bank.bankaccountsystem.account;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class RateServiceTest {

    @Mock
    private CurrencyExchangeRateRepository currencyExchangeRateRepositoryMock;
    private RateService rateService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        rateService = new RateService(currencyExchangeRateRepositoryMock);
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void shoudProperCalculateRate(CurrencyCode source, CurrencyCode destination, BigDecimal mid, BigDecimal expected) {

        //given
        Optional<CurrencyExchangeRate> pl = Optional.empty();
        Optional<CurrencyExchangeRate> usd = Optional.of(CurrencyExchangeRate.builder()
            .mid(mid)
            .build());

        BDDMockito.given(currencyExchangeRateRepositoryMock.findRateFor(eq(CurrencyCode.PLN), any(LocalDate.class))).willReturn(pl);
        BDDMockito.given(currencyExchangeRateRepositoryMock.findRateFor(eq(CurrencyCode.USD), any(LocalDate.class))).willReturn(usd);

        //when
        BigDecimal currencyRate = rateService.getCurrencyRate(source, destination, LocalDate.now());

        ///then
        assertTrue(expected.compareTo(currencyRate) == 0);
    }

    private static Stream<Arguments> testCases() {
        return Stream.of(

            Arguments.of(CurrencyCode.PLN, CurrencyCode.PLN, BigDecimal.ONE, BigDecimal.ONE),
            Arguments.of(CurrencyCode.PLN, CurrencyCode.USD, BigDecimal.TEN, BigDecimal.TEN),
            Arguments.of(CurrencyCode.USD, CurrencyCode.PLN, BigDecimal.TEN, BigDecimal.valueOf(0.1))

        );
    }

}