package com.bank.bankaccountsystem.validators;

import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class AgeLimitValidatorImplTest {

    private final AgeLimitValidatorImpl validator = new AgeLimitValidatorImpl();

    @ParameterizedTest
    @MethodSource("testCases")
    void testCases(String pesel, boolean result) {
        //when
        boolean valid = validator.isValid(pesel, null);
        //then
        Assertions.assertEquals(result, valid);
    }

    /// all pesels is valid
    private static Stream<Arguments> testCases() {
        return Stream.of(
            Arguments.of("02321469108", true), //21 lat
            Arguments.of("06292091829", false),// 17 lat
            Arguments.of("05310280812", true)); // 18 lat
    }

}